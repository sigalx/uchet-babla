<?php

require_once dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'bootstrap.php';
require_once YII_ROOT.DIRECTORY_SEPARATOR.'yii.php';

$config = dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.
	'app'.DIRECTORY_SEPARATOR.'protected'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'web.php';

Yii::createWebApplication( $config )->run();
