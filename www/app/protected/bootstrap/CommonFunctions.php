<?php

function startsWith( $source, $head )
{
	return $head === "" || strpos( $source, $head ) === 0;
}

function endsWith( $source, $tail )
{
	return $tail === "" || substr( $source, -strlen( $tail ) ) === $tail;
}

function unixtimeToDatetime( $unixtime )
{
	return date( 'Y-m-d H:i:s', $unixtime );
}

function combineArray( $keys, $values = null )
{
	if ( !$values )
		$values = $keys;

	return array_combine( $keys, $values );
}

function ipaddrToInt( $ipaddr )
{
	$matches = null;
	if ( !preg_match( '/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/', $ipaddr, $matches ) )
		return false;

	if ( count( $matches ) != 5 )
		return false;

	$result = 0;

	for ( $n = 1; $n < 5; $n++ )
		$result += intval( $matches[ $n ] ) * pow( 256, 4 - $n );

	return $result;
}
