<?php

class VkAppIdentity extends BaseIdentity
{
	public function getIdentityType()
	{
		return 'vkapp_identity';
	}

	// from vk site
	protected static function authOpenAPIMember()
	{
		$session = array();
		$member = false;
		$valid_keys = array( 'expire', 'mid', 'secret', 'sid', 'sig' );

		if ( isset( $_COOKIE[ 'vk_app_'.Yii::app()->params[ 'vk_app_id' ] ] ) &&
			( $app_cookie = $_COOKIE[ 'vk_app_'.Yii::app()->params[ 'vk_app_id' ] ] )
		)
		{
			$session_data = explode( '&', $app_cookie, 10 );
			foreach ( $session_data as $pair )
			{
				list( $key, $value ) = explode( '=', $pair, 2 );
				if ( empty( $key ) || empty( $value ) || !in_array( $key, $valid_keys ) )
					continue;
				$session[ $key ] = $value;
			}
			foreach ( $valid_keys as $key )
				if ( !isset( $session[ $key ] ) )
					return $member;

			ksort( $session );

			$sign = '';
			foreach ( $session as $key => $value )
				if ( $key != 'sig' )
					$sign .= ( $key.'='.$value );

			$sign .= Yii::app()->params[ 'vk_app_secret' ];
			$sign = md5( $sign );

			if ( $session[ 'sig' ] == $sign && $session[ 'expire' ] > time() )
			{
				$member = array(
					'id' => intval( $session[ 'mid' ] ),
					'secret' => $session[ 'secret' ],
					'sid' => $session[ 'sid' ]
				);
			}
		}

		return $member;
	}

	public function authenticate()
	{
		$this->errorCode = self::ERROR_USERNAME_INVALID;

		if ( $vk_user = static::authOpenAPIMember() )
		{
			$user = Users::model()->findByVkontakteId( $vk_user[ 'id' ] );

			if ( !$user )
			{
				$ch = curl_init( 'https://api.vk.com/method/users.get?user_ids='.$vk_user[ 'id' ].'&fields=screen_name' );
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Accept-Language: ru,en-us' ) );
				$vk_user_info = curl_exec( $ch );
				curl_close( $ch );

				$vk_user_info = json_decode( $vk_user_info );
				$vk_user_info = $vk_user_info->response[ 0 ];

				while ( Users::model()->findByLogin( $vk_user_info->screen_name ) )
					$vk_user_info->screen_name .= sprintf( '%u', crc32( $vk_user_info->screen_name ) );

				$user = Users::create( Users::authenticated_role_id, $vk_user_info->screen_name );

				$user->vkontakte_id = $vk_user[ 'id' ];
				$user->name = $vk_user_info->first_name.' '.$vk_user_info->last_name;

				if ( !$user->save() )
					die( 'Cannot save user information.' );
			}

			return $this->authorize( $user );
		}

		return false;
	}
}