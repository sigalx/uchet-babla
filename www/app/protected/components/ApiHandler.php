<?php

require_once Yii::getPathOfAlias('application.extensions.JsonRpc.Base') . DIRECTORY_SEPARATOR . 'Rpc.php';
require_once Yii::getPathOfAlias('application.extensions.JsonRpc.Base') . DIRECTORY_SEPARATOR . 'Request.php';
require_once Yii::getPathOfAlias('application.extensions.JsonRpc.Base') . DIRECTORY_SEPARATOR . 'Response.php';

use \JsonRpc\Base\Rpc;

class ApiHandler extends CApplicationComponent
{
    public $error;

    /**
     * @param string $message
     * @return null
     */
    protected function returnParamError($message)
    {
        $this->error = array(
            'code' => Rpc::ERR_PARAMS,
            'message' => $message,
        );
        return null;
    }

    /**
     * @param string $login
     * @param null|string $password
     * @param string $username
     * @return array
     */
    public function createUser($login, $password = null, $username = null)
    {
        if (Users::model()->findByLogin($login)) {
            return $this->returnParamError('User with specified login already exists');
        }

        $roleId = Users::authenticated_role_id;
        $user = Users::create($roleId, $login, $password);

        if (!$user) {
            return $this->returnParamError('User is not created');
        }

        $user->name = $username;
        $user->save();

        return array('user_info' => array(
            'id' => $user->id,
            'username' => $user->name,
        ));
    }

    /**
     * @param string $source
     * @param integer $source_id
     * @return array
     */
    public function findAuthServiceLink($source, $source_id)
    {
        if ($source !== 'vkontakte') {
            return $this->returnParamError('Specified source is not supported');
        }

        $user = Users::model()->findByVkontakteId($source_id);

        if (!$user) {
            return $this->returnParamError('Auth service link with specified parameters has not been found');
        }

        return array('user_info' => array(
            'id' => $user->id,
            'username' => $user->name,
        ));
    }

    /**
     * @param integer $user_id
     * @param string $source
     * @param string $source_id
     * @return array
     */
    public function createAuthServiceLink($user_id, $source, $source_id)
    {
        if ($source !== 'vkontakte') {
            return $this->returnParamError('Specified source is not supported');
        }

        $user = Users::model()->findByPk($user_id);

        if (!$user) {
            return $this->returnParamError('User with specified id has not been found');
        }

        $user->vkontakte_id = $source_id;

        if (!$user->save()) {
            $error = $user->errors;
            $error = reset($error);
            $error = reset($error);
            return $this->returnParamError($error);
        }

        return array();
    }

    /**
     * @param integer $user_id
     * @return array[]
     * @throws CException
     */
    public function getUserInfo($user_id)
    {
        $user = Users::model()->findByPk($user_id);

        if (!$user) {
            return $this->returnParamError('User with specified id has not been found');
        }

        return array('user_info' => array(
            'username' => $user->getName(),
            'email' => $user->email,
        ));
    }

    /**
     * @param integer $user_id
     * @param stdClass $user_info
     * @return array|null
     */
    public function updateUserInfo($user_id, stdClass $user_info)
    {
        $user = Users::model()->findByPk($user_id);

        if (!$user) {
            return $this->returnParamError('User with specified id has not been found');
        }

        $user->attributes = (array)$user_info;
        if (!$user->save()) {
            $error = $user->errors;
            $error = reset($error);
            $error = reset($error);
            return $this->returnParamError($error);
        }

        return array();
    }

    /**
     * @param integer $user_id
     * @return array[]
     */
    public function getSummaryArrears($user_id)
    {
        $user = Users::model()->findByPk($user_id);

        if (!$user) {
            return $this->returnParamError('User with specified id has not been found');
        }

        Transfers::model()->setChiperKey($user->salt);
        $arrears = array_map(function ($item) {
            return array(
                'user_id' => intval($item->user->id),
                'username' => $item->user->getName(),
                'amount' => round(abs($item->value)),
                'currency' => 'RUB',
                'relation' => $item->value < 0 ? 'creditor' : 'debtor',
            );
        }, Transfers::model()->getTotalAsObject());

        $arrears = array_filter($arrears, function ($item) {
            return $item['amount'] >= 1;
        });

        return array('arrears_info' => array_values($arrears));
    }

    /**
     * @param integer $source_user_id
     * @param integer[] $target_user_ids
     * @param double $amount
     * @param string $currency
     * @param string $comment
     * @return array
     * @throws Exception
     */
    public function createTransfer($source_user_id, array $target_user_ids, $amount, $currency, $comment)
    {
        $identity = new NoneIdentity($source_user_id);
        $identity->authenticate();

        if (!Yii::app()->user->login($identity)) {
            return $this->returnParamError('User with specified id has not been found');
        }

        if ($currency != 'RUB') {
            return $this->returnParamError('Currency is not supported');
        }

        $transfer = new Transfers;

        if (!$transfer->create(array(
            'source_user_id' => $source_user_id,
            'target_user_id' => $target_user_ids,
            'target_coefs' => array_fill(0, count($target_user_ids), 1),
            'value' => $amount,
            'comment' => $comment,
        ))
        ) {
            $error = 'Transfer has not been created';
            if ($transfer->errors) {
                $error = $transfer->errors;
                $error = reset($error);
                $error = reset($error);
            }
            return $this->returnParamError($error);
        }

        return array();
    }

    /**
     * @param integer $user_id
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getTransferHistory($user_id, $limit, $offset = 0)
    {
        $user = Users::model()->findByPk($user_id);

        if (!$user) {
            return $this->returnParamError('User with specified id has not been found');
        }

        Transfers::model()->setChiperKey($user->salt);
        $transfer_history = Transfers::model()->getHistory(intval($limit), intval($offset));
        return array(
            'transfer_history' => array_map(function (Transfers $transfer) {
                return array(
                    'timestamp' => $transfer->timestamp,
                    'source_user_id' => $transfer->source_user_id,
                    'source_username' => Users::model()->findByPk($transfer->source_user_id)->getName(),
                    'target_user_id' => $transfer->target_user_id,
                    'target_username' => Users::model()->findByPk($transfer->target_user_id)->getName(),
                    'amount' => $transfer->value,
                    'currency' => 'RUB',
                    'comment' => $transfer->comment,
                );
            }, $transfer_history),
            'total_count' => Transfers::model()->getHistoryCount());
    }

    public function getAvailableCurrencies()
    {
        return array('currency_list' => array(
            'RUB',
        ));
    }


//    public function createAccessToken($email)
//    {
//        $user = Users::model()->findByPk($user_id);
//
//        if (!$user) {
//            return $this->returnParamError('User with specified id has not been found');
//        }
//
//        $session = Sessions::create($user_id, $states[ 'auth_hash' ], $states[ 'identity_type' ], $states[ 'short_session' ] ? time() + 10 * 60 : null ) ) // 10 min
//
//        return array('user_info' => array(
//            'id' => intval($user->id),
//            'login' => $user->login,
//            'name' => $user->name,
//            'email' => $user->email,
//            'register_time' => intval($user->register_time),
//        ));
//    }


}
