<?php

class XMysqlSchema extends CMysqlSchema
{
	public $columnTypes = array
	(
		'pk' => 'int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY',
		'string' => 'varchar(255)',
		'text' => 'text',
		'integer' => 'int',
		'float' => 'float',
		'decimal' => 'decimal',
		'datetime' => 'datetime',
		'timestamp' => 'timestamp',
		'time' => 'time',
		'date' => 'date',
		'binary' => 'blob',
		'boolean' => 'tinyint(1)',
		'money' => 'decimal(19,4)',
	);

}
