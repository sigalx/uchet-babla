<?php

class NoneIdentity extends BaseIdentity
{
    public $user_id;

    public function getIdentityType()
    {
        return 'none_identity';
    }

    public function __construct( $user_id )
    {
        $this->user_id = $user_id;
    }

    public function authenticate()
    {
        $user = Users::model()->findByPk($this->user_id);

        if ( !$user )
            return false;

        return $this->authorize( $user );
    }
}
