<?php

abstract class BaseIdentity extends CBaseUserIdentity
{
	public $id;
	public $name;

	abstract public function getIdentityType();

	protected function authorize( $user )
	{
		$this->id = $user->id;
		$this->name = !empty( $user->name ) ? $user->name : $user->login;

		$this->setState( 'auth_hash', base64_encode( BaseModel::generateHash( $this->id.$user->login.uniqid(), $user->salt ) ) );
		$this->setState( 'disabled', $user->disabled );
		$this->setState( 'locked_till', $user->locked_till );

		$this->setState( 'identity_type', $this->getIdentityType() );

		$this->errorCode = self::ERROR_NONE;

		return true;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}
}