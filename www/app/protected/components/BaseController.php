<?php

class BaseController extends CController
{
	public $layout = '//layouts/main';
	public $breadcrumbs = array();
	public $pageTitle = '';

	public function filters()
	{
		return array( 'accessControl' );
	}

	public function accessRules()
	{
		return array(
			array( 'allow', 'users' => array( '@' ) ),
			array( 'deny' ),
		);
	}

	public function getViewPath()
	{
		$module = null;
		if ( ( $module = $this->getModule() ) === null )
			$module = Yii::app();

		return $module->getViewPath().DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$this->getId();
	}

	protected function beforeAction( $action )
	{
		if ( !parent::beforeAction( $action ) )
			return false;

//		if ( Yii::app()->request->pathInfo == $this->id.'/index' )
//			$this->redirect( '/'.$this->id );

		return true;
	}

	protected function beforeRender( $view )
	{
		if ( !parent::beforeRender( $view ) )
			return false;

		if ( empty( $this->pageTitle ) )
			$this->pageTitle = ucfirst( basename( $this->action->id ) );

		return true;
	}

	public function loadModel( $model, $value, $column = 'id', $message = null )
	{
		$record = BaseModel::model( $model )->findByAttributes( array( $column => $value ) );

		if ( $record === null )
		{
			$message ?
				$this->throwHttpNotFound( $message ) :
				$this->throwHttpNotFound();
		}

		return $record;
	}

	protected function throwHttpException( $status, $message = '' )
	{
		throw new CHttpException( $status, $message );
	}

	protected function throwHttpNotFound( $message = null )
	{
		if ( !isset( $message ) )
			$message = 'Указанной страницы не существует';

		$this->throwHttpException( 404, $message );
	}

	protected function throwInternalServerError( $message = null )
	{
		if ( !isset( $message ) )
			$message = 'Произошла ошибка, повторите запрос позднее или <a href="mailto:'.Yii::app()->params[ 'admin_email' ].'">свяжитесь с администратором</a>';

		$this->throwHttpException( 500, $message );
	}
}

;
