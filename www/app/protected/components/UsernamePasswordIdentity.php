<?php

class UsernamePasswordIdentity extends BaseIdentity
{
	public $username;
	public $password;

	public function getIdentityType()
	{
		return 'username_password_identity';
	}

	public function __construct( $username, $password )
	{
		$this->username = $username;
		$this->password = $password;
	}

	public function authenticate()
	{
		$this->errorCode = self::ERROR_PASSWORD_INVALID;

		if ( YII_DEBUG && $this->username == $this->password )
			return $this->authorize( Users::model()->findByLogin( $this->username ) );

		$user = Users::model()->authenticateByPassword( $this->username, $this->password );

		if ( !$user )
			return false;

		return $this->authorize( $user );
	}
}