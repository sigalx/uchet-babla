<?php

class XDbMigration extends CDbMigration
{
	public function __construct()
	{
		if ($this->dbConnection->driverName == 'sqlite')
			define('MIGRATE_SQLITE', true);
		if ($this->dbConnection->driverName == 'mysql')
			define('MIGRATE_MYSQL', true);
	}

}
