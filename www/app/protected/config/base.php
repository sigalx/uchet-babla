<?php

require_once dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'bootstrap'.DIRECTORY_SEPARATOR.'CommonFunctions.php';

return array
(
	'basePath' => dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..',
	'runtimePath' => dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'runtime',

	'name' => 'Учёт Бабла',

	'sourceLanguage' => 'ru_ru',
	'language' => 'ru_ru',

	'timeZone' => 'Europe/Moscow',

	'preload' => array( 'log' ),

	'import' => array
	(
		'application.models.*',
		'application.components.*',
	),

	'modules' => array
	(
	),

	'components' => array
	(
		'db' => array
		(
			'connectionString' => DB_CONNECTION_STRING,
			'emulatePrepare' => true,
			'username' => DB_USERNAME,
			'password' => DB_PASSWORD,
			'charset' => 'utf8',

			'driverMap' => array(
				'pgsql' => 'CPgsqlSchema',    // PostgreSQL
				'mysqli' => 'XMysqlSchema',   // MySQL
				'mysql' => 'XMysqlSchema',    // MySQL
				'sqlite' => 'CSqliteSchema',  // sqlite 3
				'sqlite2' => 'CSqliteSchema', // sqlite 2
				'mssql' => 'CMssqlSchema',    // Mssql driver on windows hosts
				'dblib' => 'CMssqlSchema',    // dblib drivers on linux (and maybe others os) hosts
				'sqlsrv' => 'CMssqlSchema',   // Mssql
				'oci' => 'COciSchema',        // Oracle driver
			),
		),

		'log' => array
		(
			'class' => 'CLogRouter',
			'routes' => array
			(
				array
				(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),

		'email' => array(
			'class' => 'application.extensions.email.Email',
			'delivery' => 'php',
			'type' => 'text/plain; charset=utf-8',
			'from' => 'Учёт Бабла <noreply@sigalx.ru>', // TODO: move to app params
			'layout' => 'email',
		),

        'api' => array(
            'class' => 'application.components.ApiHandler',
        ),
	),

	'params' => array
	(
		'admin_email' => 'sigalx@sigalx.ru',

		'salt' => APP_SALT,
		'cookie_ttl' => 86400,

		'vk_app_id' => VK_APP_ID,
		'vk_app_secret' => VK_APP_SECRET,
	),
);