<?php

return array_merge_recursive( require 'base.php',
array
(
	'import' => array
	(
		'application.web.helpers.*',
		'application.web.widgets.*',
	),

	'modules' => array
	(
	),

	'components' => array
	(
		'assetManager' => array
		(
			'class' => 'XAssetManager',
			'basePath' => dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'assets',
		),

		'clientScript' => array
		(
			'scriptMap' => array
			(
				'jquery.js' => '//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.js',
				'jquery.min.js' => '//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js',

				'jquery-ui.js' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.js',
				'jquery-ui.min.js' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js',

				'jquery-ui.css' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css',
				'jquery-ui.min.css' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css',
			),
			'packages' => array
			(
				'jquery' => array
				(
					'js' => array( YII_DEBUG ? 'jquery.js' : 'jquery.min.js' )
				),
				'jquery.ui' => array
				(
					'js' => array( YII_DEBUG ? 'jquery-ui.js' : 'jquery-ui.min.js' ),
					'css' => array( YII_DEBUG ? 'jquery-ui.css' : 'jquery-ui.min.css' ),
					'depends' => array( 'jquery' )
				)
			),
		),

		'urlManager' => array
		(
			'urlFormat' => YII_DEBUG ? 'get' : 'path',
			'showScriptName' => YII_DEBUG,
			'rules' => array
			(
				'<controller:\w+>/<id:\d+>' => '<controller>',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<uid:\w+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
		),

		'user' => array
		(
			'class' => 'WebUser',
			'allowAutoLogin' => true,
			'loginUrl' => '/users/login',
		),

		'session' => array
		(
			'class' => 'CDbHttpSession',
		),

		'errorHandler' => array
		(
			'errorAction' => '/site/error',
		),

		'log' => array
		(
			'routes' => array
			(
				array
				(
					'class' => 'CWebLogRoute',
					'categories' => 'system.db.CDbCommand',
				),

			),
		),
	),
)

);
