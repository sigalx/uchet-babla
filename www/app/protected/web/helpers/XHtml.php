<?php

class XHtml extends CHtml
{
	public static $before_required_input;
	public static $after_required_input;

	// works only with late static binding
	protected static function activeInputField( $type, $model, $attribute, $html_options = array() )
	{
		$div_options = array( 'class' => 'input' );

		if ( !empty( $html_options[ 'class' ] ) )
			$div_options[ 'class' ] .= ' '.$html_options[ 'class' ];

		if ( $model->hasErrors( $attribute ) )
			static::addErrorCss( $div_options );

		$input_rendered = parent::activeInputField( $type, $model, $attribute, $html_options );

		if ( !in_array( $type, array( 'checkbox' ) ) )
			$input_rendered = static::tag( 'div', $div_options, $input_rendered );

		if ( $model->isAttributeRequired( $attribute ) )
			return
				static::$before_required_input.
				$input_rendered.
				static::$after_required_input;

		return $input_rendered;
	}

	public static function activeTextArea( $model, $attribute, $html_options = array() )
	{
		static::resolveNameID( $model, $attribute, $html_options );
		static::clientChange( 'change', $html_options );

		$div_options = array( 'class' => 'textarea' );

		if ( !empty( $html_options[ 'class' ] ) )
			$div_options[ 'class' ] .= ' '.$html_options[ 'class' ];

		if ( $model->hasErrors( $attribute ) )
			static::addErrorCss( $div_options );

		if ( isset( $html_options[ 'value' ] ) )
		{
			$text = $html_options[ 'value' ];
			unset( $html_options[ 'value' ] );
		}
		else
			$text = static::resolveValue( $model, $attribute );

		$input_rendered = static::tag( 'textarea', $html_options, isset( $html_options[ 'encode' ] ) && !$html_options[ 'encode' ] ? $text : static::encode( $text ) );
		$input_rendered = static::tag( 'div', $div_options, $input_rendered );

		if ( $model->isAttributeRequired( $attribute ) )
			return
				static::$before_required_input.
				$input_rendered.
				static::$after_required_input;

		return $input_rendered;
	}

	public static function activeDatetimeField( $model, $attribute, $html_options = array() )
	{
		static::resolveNameID( $model, $attribute, $html_options );
		static::clientChange( 'change', $html_options );

		return static::activeInputField( 'datetime', $model, $attribute, $html_options );
	}

	public static function activeDropDownList( $model, $attribute, $data, $html_options = array() )
	{
		if ( isset( $html_options[ 'no-wrapper' ] ) )
			return parent::activeDropDownList( $model, $attribute, $data, $html_options );

		$div_options = array( 'class' => 'input' );

		if ( $model->hasErrors( $attribute ) )
			static::addErrorCss( $div_options );

		$input_rendered = static::tag( 'div', $div_options,
			parent::activeDropDownList( $model, $attribute, $data, $html_options ) );

		if ( $model->isAttributeRequired( $attribute ) )
			return
				static::$before_required_input.
				$input_rendered.
				static::$after_required_input;

		return $input_rendered;
	}

	public static function activeCheckBoxList( $model, $attribute, $data, $html_options = array() )
	{
		if ( isset( $html_options[ 'no-wrapper' ] ) )
			return parent::activeCheckBoxList( $model, $attribute, $data, $html_options );

		$div_options = array( 'class' => 'checkbox-list' );

		if ( $model->hasErrors( $attribute ) )
			static::addErrorCss( $div_options );

		$input_rendered = static::tag( 'div', $div_options,
			parent::activeCheckBoxList( $model, $attribute, $data, $html_options ) );

		if ( $model->isAttributeRequired( $attribute ) )
			return
				static::$before_required_input.
				$input_rendered.
				static::$after_required_input;

		return $input_rendered;
	}

	public static function activeLabel( $model, $attribute, $html_options = array() )
	{
		if ( isset( $html_options[ 'no-wrapper' ] ) )
			return parent::activeLabel( $model, $attribute, $html_options );

		$div_options = array( 'class' => 'label' );

		if ( $model->hasErrors( $attribute ) )
			static::addErrorCss( $div_options );

		return static::tag( 'div', $div_options,
			parent::activeLabel( $model, $attribute, $html_options ) );
	}

	public static function submitButton( $label = 'submit', $html_options = array() )
	{
		if ( !isset( $html_options[ 'class' ] ) )
			$html_options[ 'class' ] = '';
		$html_options[ 'class' ] = ' button submit';

		return parent::submitButton( $label, $html_options );
	}

	public static function encode( $text )
	{
		return str_replace(
			array( "\r\n", "\t" ),
			array( '<br />', '&#09;' ),
			CHtml::encode( $text )
		);
	}
}

;
