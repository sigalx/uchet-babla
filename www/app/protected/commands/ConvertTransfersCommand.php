<?php

class ConvertTransfersCommand extends CConsoleCommand
{
    public function run( $args )
    {
        header( 'Content-Type: text/plain; charset=utf-8' );

        $users = Users::model()->findAll();

        foreach ($users as $user) {
            echo 'Processing user "' . $user->login . '"...' . PHP_EOL;
            $transfers = new Transfers;
            $transfers->setChiperKey($user->salt);
            $transfers = $transfers->getHistory();
            $count = 0;
            foreach ($transfers as $transfer) {
                if (true || empty($transfer->data2)) {
                    $transfer->data2 = BaseModel::encrypt2( json_encode( $transfer->getTransferAttributes() ), $user->salt );
                    $transfer->save();
                    $count++;
                    if ($count % 10 == 0) {
                        echo $count . ' transfers processed.' . PHP_EOL;
                    }
                }
            }
        }

        echo 'All done!'.PHP_EOL;
    }

}

;
