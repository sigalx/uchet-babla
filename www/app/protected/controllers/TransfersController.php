<?php

class TransfersController extends BaseController
{
	public $pageTitle = 'Мои переводы';

	public function init()
	{
		parent::init();

		$this->breadcrumbs = array(
			$this->pageTitle => array( 'index' ),
		);
	}

	public function accessRules()
	{
		return array_merge(
			array(),
			parent::accessRules()
		);
	}

	public function filters()
	{
		return array_merge(
			array(
				'postOnly + delete'
			),
			parent::filters()
		);
	}

	public function actionIndex()
	{
		$pages = new CPagination( Transfers::model()->getHistoryCount() );
		$pages->pageSize = 10;

		$transfers = Transfers::model()->getHistory( $pages->limit, $pages->offset );

		$this->render( 'index', array(
			'transfers' => $transfers,
			'pages' => $pages,
		) );
	}

	public function actionTotal()
	{
		$this->render( 'total', array(
			'creditors' => Transfers::model()->getCreditors(),
			'debtors' => Transfers::model()->getDebtors(),
		) );
	}

	public function actionView( $id )
	{
		$model = Transfers::model()->getTransfer( $id );

		if ( $model === null )
			throwHttpNotFound( 'Запрошенный перевод не существует' );

		$this->render( 'view', array(
			'model' => $model,
		) );
	}

	public function actionCreate( $party_id = null, $source_user_id = null, $target_user_id = null )
	{
		$party_members = array( Yii::app()->user->session->user );
		$source_user = null;
		$target_user = null;

		if ( isset( $party_id ) )
		{
			$party = parent::loadModel( 'Parties', $party_id );
			$party_members = $party->users;
		}

		if ( isset( $source_user_id ) )
		{
			$source_user = parent::loadModel( 'Users', $source_user_id );
			$party_members[ ] = $source_user;
		}

		if ( isset( $target_user_id ) )
		{
			$target_user = parent::loadModel( 'Users', $target_user_id );
			$party_members[ ] = $target_user;
		}

		if ( empty( $party_members ) )
			$this->throwHttpException( 400, 'Неверный запрос. Не указаны обязательные параметры.' );

		$model = new Transfers;

		$this->performAjaxValidation( $model );

		if ( isset( $_POST[ 'Transfers' ] ) )
		{
			if ( $model->create( $_POST[ 'Transfers' ] ) )
			{
				Yii::app()->user->setFlash( 'system-highlight', 'Ваш перевод успешно создан.<br />'.XHtml::link( 'Создать еще один', array( 'create', 'party_id' => $party_id ) ) );
				$this->redirect( array( 'index' ) );
			}
		}

		$this->render( 'create', array(
			'model' => $model,
			'party_members' => $party_members,
			'source_user' => $source_user,
			'target_user' => $target_user,
		) );
	}

	public function actionCreateMultiple( $party_id )
	{
		$party = parent::loadModel( 'Parties', $party_id );

		$model = new Transfers;

		$this->performAjaxValidation( $model );

		if ( isset( $_POST[ 'Transfers' ] ) )
		{
			if ( $model->create( $_POST[ 'Transfers' ] ) )
			{
				Yii::app()->user->setFlash( 'system-highlight', 'Ваш перевод успешно создан.<br />'.XHtml::link( 'Создать еще один', array( 'create', 'party_id' => $party_id ) ) );
				$this->redirect( array( 'index' ) );
			}
		}

		$this->render( 'create_multiple', array(
			'model' => $model,
			'party_members' => $party->users,
		) );
	}

	protected function performAjaxValidation( $model )
	{
		if ( isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'transfers-form' )
		{
			echo CActiveForm::validate( $model );
			Yii::app()->end();
		}
	}
}

;
