<?php

require_once Yii::getPathOfAlias('application.extensions.JsonRpc') . DIRECTORY_SEPARATOR . 'Server.php';
require_once Yii::getPathOfAlias('application.extensions.JsonRpc.Transport') . DIRECTORY_SEPARATOR . 'BasicServer.php';

class ApiController extends BaseController
{
    public $layout = false;

    public function accessRules()
    {
        return array(
            array('allow'),
        );
    }

    protected function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute) {
                $route->enabled = false;
            }
        }

        return true;
    }

    public function actionIndex()
    {
        $server = new JsonRpc\Server(Yii::app()->api);

        if (Yii::app()->request->isPostRequest) {
            header('Content-Type: application/json');
            $server->receive();
            Yii::app()->end();
        }

        echo 'SMD here';
        Yii::app()->end();
    }

}
