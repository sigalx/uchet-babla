<?php

class SiteController extends BaseController
{
	public $pageTitle = 'Welcome to Учёт Бабла';

	public function init()
	{
		parent::init();

		$this->breadcrumbs = array(
			$this->pageTitle => array( 'index' ),
		);
	}

	public function accessRules()
	{
		return array_merge(
			array(
				array( 'allow',
					/*'actions' => array('error')*/ ),
			),
			parent::accessRules()
		);
	}

	public function actions()
	{
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	public function actionIndex()
	{
		$this->render( 'index' );
	}

	public function actionError()
	{
		if ( $error = Yii::app()->errorHandler->error )
		{
			if ( Yii::app()->request->isAjaxRequest )
				echo $error[ 'message' ]; // TODO
			else
				$this->render( 'error', $error );
		}
	}
	
	public function actionSendNotifications()
	{
		$users = Users::model()->findAllByAttributes( array(),
			'(`disabled` IS NULL OR `disabled` = 0 ) AND ( `locked_till` IS NULL OR `locked_till` < :current_timestamp ) AND `email` IS NOT NULL',
			array( ':current_timestamp' => time() ) );
		foreach ( $users as $user )
		{
			$transfers = new Transfers;
			$transfers->chiperkey = $user->salt;
			$creditors = $transfers->getCreditors();
			
			if ( empty( $creditors ) )
				continue;
			
			try
			{
				$email = Yii::app()->email;
				$email->to = $user->email;
				$email->view = 'notification';
				$email->viewVars = array(
					'user' => $user,
					'creditors' => $creditors,
				);
				$email->send();
			}
			catch (Exception $exception)
			{
				echo Yii::t( 'app', 'Something is going wrong when trying to send notification to {email}: {message}', array( '{email}' => $user->email, '{message}' => $exception->getMessage() ) );
			}
		}
	}

	public function actionContact()
	{
		$model = new ContactForm;
		if ( isset( $_POST[ 'ContactForm' ] ) )
		{
			$model->attributes = $_POST[ 'ContactForm' ];
			if ( $model->validate() )
			{
				$name = '=?UTF-8?B?'.base64_encode( $model->name ).'?=';
				$subject = '=?UTF-8?B?'.base64_encode( $model->subject ).'?=';
				$headers = "From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail( Yii::app()->params[ 'adminEmail' ], $subject, $model->body, $headers );
				Yii::app()->user->setFlash( 'contact', 'Thank you for contacting us. We will respond to you as soon as possible.' );
				$this->refresh();
			}
		}
		$this->render( 'contact', array( 'model' => $model ) );
	}
}

;
