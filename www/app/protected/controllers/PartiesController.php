<?php

class PartiesController extends BaseController
{
	public $pageTitle = 'Мои партии';

	public function init()
	{
		parent::init();

		$this->breadcrumbs = array(
			$this->pageTitle => array( 'index' ),
		);
	}

	public function accessRules()
	{
		return array_merge(
			array(),
			parent::accessRules()
		);
	}

	public function filters()
	{
		return array_merge(
			array(
				'postOnly + delete'
			),
			parent::filters()
		);
	}

	public function actionIndex()
	{
		$this->render( 'index', array(
			'parties' => Yii::app()->user->session->user->parties,
		) );
	}

	public function actionView( $uid = null )
	{
		$model = $this->loadParty( null, $uid );

		if ( !$model->isPartyMember() )
			$this->throwHttpException( 403, 'Чтобы просматривать страницу партии, нужно быть ее членом.' );

		$this->render( 'view', array(
			'model' => $model,
		) );
	}

	public function actionInvite( $uid )
	{
		$model = $this->loadParty( null, $uid );

		if ( !$model->isPartyMember() )
			$this->throwHttpException( 403, 'Чтобы просматривать страницу партии, нужно быть ее членом.' );

		$this->render( 'invite', array(
			'model' => $model,
		) );
	}

	public function actionJoin( $uid )
	{
		$model = $this->loadParty( null, $uid );

		if ( $model->isPartyMember() )
			$this->throwHttpException( 403, 'Вы уже вступили в эту партию.' );

		if ( Yii::app()->request->getIsPostRequest() )
		{
			if ( $model->joinUser() )
				$this->redirect( array( 'view', 'uid' => $model->uri_name ) );
		}

		$this->render( 'join', array(
			'model' => $model,
		) );
	}

	public function actionCreate()
	{
		$model = new Parties;

		$this->performAjaxValidation( $model );

		if ( isset( $_POST[ 'Parties' ] ) )
		{
			if ( $model->create( $_POST[ 'Parties' ] ) )
				$this->redirect( array( 'view', 'uid' => $model->uri_name ) );
		}

		$this->render( 'create', array(
			'model' => $model,
		) );
	}

	public function loadParty( $id = null, $uri_name = null )
	{
		/**/
		if ( isset( $id ) )
			return parent::loadModel( 'Parties', $id );
		else if ( isset( $uri_name ) )
			return parent::loadModel( 'Parties', $uri_name, 'uri_name' );

		$this->throwHttpException( 400, 'Отсутствует идентификатор партии.' );
	}

	protected function performAjaxValidation( $model )
	{
		if ( isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'parties-form' )
		{
			echo CActiveForm::validate( $model );
			Yii::app()->end();
		}
	}
}
