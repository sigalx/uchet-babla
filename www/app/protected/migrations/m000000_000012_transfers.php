<?php

class m000000_000012_transfers extends XDbMigration
{
	public function safeUp()
	{
		$this->createTable( 'transfers', array(
			'id' => 'pk',
			'hash' => 'char(128) NOT NULL',
			'data' => 'binary NOT NULL'
		), defined( 'MIGRATE_MYSQL' ) ? 'AUTO_INCREMENT = 100' : '' );

		$this->createIndex( 'transfers_hash_idx', 'transfers', 'hash', false );

		return true;
	}

	public function safeDown()
	{
		$this->dropTable( 'transfers' );

		return true;
	}
}