<?php

class m000000_000001_init extends XDbMigration
{
	public function safeUp()
	{
		$this->createTable( 'roles', array(
			'id' => 'pk',
			'name' => 'CHAR(50) NOT NULL',
			'description' => 'string DEFAULT NULL',
		), defined( 'MIGRATE_MYSQL' ) ? 'AUTO_INCREMENT = 100' : '' );

		$this->createIndex( 'roles_name_unique_idx', 'roles', 'name', true );

		$this->createTable( 'users', array(
			'id' => 'pk',
			'role_id' => 'INT UNSIGNED NOT NULL',
			'login' => 'CHAR(50) NOT NULL',
			'hash' => 'CHAR(128) NOT NULL',
			'salt' => 'CHAR(128) NOT NULL',
			'register_time' => 'BIGINT UNSIGNED NOT NULL',
			'cannot_change_password' => 'boolean DEFAULT NULL',
			'password_expiration' => 'BIGINT UNSIGNED DEFAULT NULL',
			'disabled' => 'boolean DEFAULT NULL',
			'locked_till' => 'BIGINT UNSIGNED DEFAULT NULL',
			'name' => 'string DEFAULT NULL',
			'email' => 'string DEFAULT NULL',
			'vkontakte_id' => 'BIGINT UNSIGNED DEFAULT NULL',
		), defined( 'MIGRATE_MYSQL' ) ? 'AUTO_INCREMENT = 100' : '' );

		$this->createIndex( 'users_login_unique_idx', 'users', 'login', true );
		$this->createIndex( 'users_email_unique_idx', 'users', 'email', true );
		$this->createIndex( 'users_vkontakte_id_unique_idx', 'users', 'vkontakte_id', true );

		$this->createIndex( 'users_role_id_idx', 'users', 'role_id', false );
		$this->createIndex( 'users_register_time_idx', 'users', 'register_time', false );

		if ( !defined( 'MIGRATE_SQLITE' ) )
			$this->addForeignKey(
				'users_ibfk_1', 'users', 'role_id',
				'roles', 'id',
				'CASCADE', 'CASCADE'
			);

		$this->createTable( 'sessions', array(
			'id' => 'pk',
			'user_id' => 'INT UNSIGNED NOT NULL',
			'hash' => 'CHAR(128) NOT NULL',
			'identity_type' =>
			( !defined( 'MIGRATE_SQLITE' ) ?
				'ENUM(\'username_password_identity\', \'vkapp_identity\')' :
				'string'
			).' NOT NULL',
			'start_time' => 'BIGINT UNSIGNED NOT NULL',
			'close_time' => 'BIGINT UNSIGNED DEFAULT NULL',
			'ipaddr' => 'INT UNSIGNED NOT NULL',
		), defined( 'MIGRATE_MYSQL' ) ? 'AUTO_INCREMENT = 100' : '' );

		$this->createIndex( 'sessions_hash_unique_idx', 'sessions', 'hash', true );

		$this->createIndex( 'sessions_user_id_idx', 'sessions', 'user_id', false );
		$this->createIndex( 'sessions_identity_type_idx', 'sessions', 'identity_type', false );
		$this->createIndex( 'sessions_start_time_idx', 'sessions', 'start_time', false );
		$this->createIndex( 'sessions_close_time_idx', 'sessions', 'close_time', false );
		$this->createIndex( 'sessions_ipaddr_idx', 'sessions', 'ipaddr', false );

		if ( !defined( 'MIGRATE_SQLITE' ) )
			$this->addForeignKey(
				'sessions_ibfk_1', 'sessions', 'user_id',
				'users', 'id',
				'CASCADE', 'CASCADE'
			);

		$this->insert( 'roles', array( 'id' => 1, 'name' => 'root' ) );
		$this->insert( 'roles', array( 'id' => 2, 'name' => 'administrator' ) );
		$this->insert( 'roles', array( 'id' => 10, 'name' => 'authenticated' ) );
		$this->insert( 'roles', array( 'id' => 50, 'name' => 'anonymous' ) );

		$this->insert( 'users', array(
			'id' => 1,
			'role_id' => 1,
			'login' => 'root',
			'hash' => 'root',
			'salt' => 'root',
			'register_time' => 0,
		) );

		$this->insert( 'users', array(
			'id' => 2,
			'role_id' => 50,
			'login' => 'anonymous',
			'hash' => 'anonymous',
			'salt' => 'anonymous',
			'register_time' => 0,
		) );

		$this->insert( 'users', array(
			'id' => 3,
			'role_id' => 2,
			'login' => 'admin',
			'hash' => 'admin',
			'salt' => 'admin',
			'register_time' => 0,
		) );

		$this->insert( 'users', array(
			'id' => 4,
			'role_id' => 10,
			'login' => 'user',
			'hash' => 'user',
			'salt' => 'user',
			'register_time' => 0,
		) );

		return true;
	}

	public function safeDown()
	{
		$this->dropTable( 'sessions' );
		$this->dropTable( 'users' );
		$this->dropTable( 'roles' );

		return true;
	}
}
