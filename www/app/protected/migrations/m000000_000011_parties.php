<?php

class m000000_000011_parties extends XDbMigration
{
	public function safeUp()
	{
		$this->createTable( 'parties', array(
			'id' => 'pk',
			'name' => 'string NOT NULL',
			'owner_user_id' => 'INT UNSIGNED NOT NULL',
			'hash' => 'char(128) NOT NULL',
			'register_time' => 'BIGINT UNSIGNED NOT NULL',
			'uri_name' => 'CHAR(50) NOT NULL',
			'email' => 'string DEFAULT NULL',
		), defined( 'MIGRATE_MYSQL' ) ? 'AUTO_INCREMENT = 100' : '' );

		$this->createIndex( 'parties_hash_unique_idx', 'parties', 'hash', true );

		$this->createIndex( 'parties_owner_user_id_idx', 'parties', 'owner_user_id', false );
		$this->createIndex( 'parties_register_time_idx', 'parties', 'register_time', false );

		if ( !defined( 'MIGRATE_SQLITE' ) )
			$this->addForeignKey(
				'parties_ibfk_1', 'parties', 'owner_user_id',
				'users', 'id',
				'CASCADE', 'CASCADE'
			);

		$this->createTable( 'party_members', array(
			'id' => 'pk',
			'party_id' => 'INT UNSIGNED NOT NULL',
			'user_id' => 'INT UNSIGNED NOT NULL',
			'member_since' => 'BIGINT UNSIGNED NOT NULL',
		), defined( 'MIGRATE_MYSQL' ) ? 'AUTO_INCREMENT = 100' : '' );

		$this->createIndex( 'party_members_party_id_user_id_unique_idx', 'party_members', 'party_id, user_id', true );

		$this->createIndex( 'party_members_party_id_idx', 'party_members', 'party_id', false );
		$this->createIndex( 'party_members_member_since_idx', 'party_members', 'member_since', false );

		if ( !defined( 'MIGRATE_SQLITE' ) )
		{
			$this->addForeignKey(
				'party_members_ibfk_1', 'party_members', 'party_id',
				'parties', 'id',
				'CASCADE', 'CASCADE'
			);
			$this->addForeignKey(
				'party_members_ibfk_2', 'party_members', 'user_id',
				'users', 'id',
				'CASCADE', 'CASCADE'
			);
		}

		return true;
	}

	public function safeDown()
	{
		$this->dropTable( 'party_members' );
		$this->dropTable( 'parties' );

		return true;
	}
}