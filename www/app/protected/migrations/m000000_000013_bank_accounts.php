<?php

class m000000_000013_bank_accounts extends XDbMigration
{
	public function safeUp()
	{
		$this->createTable( 'bank_accounts', array(
			'id' => 'pk',
			'user_id' => 'INT UNSIGNED NOT NULL',
			'fullname' => 'string',
			'account_number' => 'CHAR(30) NOT NULL',
			'bank_name' => 'string NOT NULL',
			'bik' => 'CHAR(30) NOT NULL',
			'correspondent_account' => 'CHAR(30) NOT NULL',
			'inn' => 'CHAR(30) NOT NULL',
			'kpp' => 'CHAR(30) NOT NULL',
		), defined( 'MIGRATE_MYSQL' ) ? 'AUTO_INCREMENT = 100' : '' );

		$this->createIndex( 'bank_accounts_user_id_unique_idx', 'bank_accounts', 'user_id', true );

		if ( !defined( 'MIGRATE_SQLITE' ) )
			$this->addForeignKey(
				'bank_accounts_ibfk_1', 'bank_accounts', 'user_id',
				'users', 'id',
				'CASCADE', 'CASCADE'
			);

		return true;
	}

	public function safeDown()
	{
		$this->dropTable( 'bank_accounts' );

		return true;
	}
}