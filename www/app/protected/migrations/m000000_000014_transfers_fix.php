<?php

class m000000_000014_transfers_fix extends XDbMigration
{
	public function safeUp()
	{
        $this->addColumn( 'transfers', 'data2', 'binary NOT NULL' );

		return true;
	}

	public function safeDown()
	{
        $this->dropColumn( 'transfers', 'data2' );

		return true;
	}
}