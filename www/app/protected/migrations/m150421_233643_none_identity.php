<?php

class m150421_233643_none_identity extends XDbMigration
{
    public function safeUp()
    {
        $this->alterColumn('sessions', 'identity_type',
			( !defined( 'MIGRATE_SQLITE' ) ?
                'ENUM(\'username_password_identity\', \'vkapp_identity\', \'none_identity\')' :
                'string'
            ).' NOT NULL');

        return true;
    }

    public function safeDown()
    {
        $this->alterColumn('sessions', 'identity_type',
            ( !defined( 'MIGRATE_SQLITE' ) ?
                'ENUM(\'username_password_identity\', \'vkapp_identity\')' :
                'string'
            ).' NOT NULL');

        return true;
    }
}
