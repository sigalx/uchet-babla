<?php

class Transfers extends BaseModel
{
	const default_signing_records = 10;

	public $timestamp;
	public $session_id;
	public $pair_transfer_id;
	public $source_user_id;
	public $target_user_id;
	public $target_coefs;
	public $value;
	public $comment;
	public $total;
	public $sign;
	public $signed_records;

	protected $_chiperkey;
	
	public function setChiperKey($value = null)
	{
		if ( !isset( $value ) )
			$value = Yii::app()->user->session->user->salt;
		$this->_chiperkey = $value;
	}

	public static function model( $className = __CLASS__ )
	{
		return parent::model( $className );
	}

	public function tableName()
	{
		return 'transfers';
	}

	public function getTransferAttributes()
	{
		return $this->getAttributes( array(
			'timestamp',
			'session_id',
			'pair_transfer_id',
			'source_user_id',
			'target_user_id',
			'target_coefs',
			'value',
			'comment',
			'total',
			'sign',
			'signed_records',
		) );
	}

	public function setTransferAttributes( $attributes )
	{
		$this->timestamp = intval( $attributes[ 'timestamp' ] );
		$this->session_id = intval( $attributes[ 'session_id' ] );
		$this->pair_transfer_id = intval( $attributes[ 'pair_transfer_id' ] );
		$this->source_user_id = intval( $attributes[ 'source_user_id' ] );
		$this->target_user_id = intval( $attributes[ 'target_user_id' ] );
		$this->target_coefs = floatval( isset( $attributes[ 'target_coefs' ] ) ? $attributes[ 'target_coefs' ] : 1 );
		$this->value = floatval( $attributes[ 'value' ] );
		$this->comment = $attributes[ 'comment' ];
		$this->total = $attributes[ 'total' ];
		$this->sign = $attributes[ 'sign' ];
		$this->signed_records = isset( $attributes[ 'signed_records' ] ) ? intval( $attributes[ 'signed_records' ] ) : null;
	}

	public function rules()
	{
		return array(
			array( 'source_user_id, target_user_id, value, comment', 'required', 'message' => 'Поле «{attribute}» должно быть заполнено' ),

			array( 'source_user_id', 'checkUserConstraint', 'message' => 'Некорректное значение поля «{attribute}»' ),
			array( 'target_user_id', 'checkUserConstraint', 'message' => 'Некорректное значение поля «{attribute}»' ),
			array( 'target_user_id', 'checkSameAsSource', 'message' => '{attribute} не может совпадать с Источником перевода' ),
			array( 'value', 'numerical',
				'min' => 0.05,
				'max' => 10000,
				'tooSmall' => 'Введеное значение слишком мало (допустимо не менее 0,01)',
				'tooBig' => 'Введеное значение слишком велико (допустимо не более 10000)',
				'message' => 'Некорректное значение поля «{attribute}»'
			),

			array( 'source_user_id, target_user_id, value, comment, target_coefs', 'safe' ),
		);
	}

	public function relations()
	{
		return array(
			'session' => array( self::BELONGS_TO, 'Sessions', 'session_id' ),
			'pair_transfer' => array( self::BELONGS_TO, 'Transfers', 'pair_transfer_id' ),
			'source_user' => array( self::BELONGS_TO, 'Users', 'source_user_id' ),
			'target_user' => array( self::BELONGS_TO, 'Users', 'target_user_id' ),
		);
	}

	public function attributeLabels()
	{
		return array(
			'timestamp' => 'Дата и время перевода',
			'source_user_id' => 'Источник перевода',
			'target_user_id' => 'Получатель перевода',
			'target_coefs' => 'Коэффициенты распределения',
			'value' => 'Сумма перевода',
			'comment' => 'Комментарий',
		);
	}

	public function checkUserConstraint( $attribute, $params )
	{
		$attribute_label = $this->attributeLabels();
		$attribute_label = $attribute_label[ $attribute ];

		if ( is_array( $this->$attribute ) )
		{
			foreach ( $this->$attribute as $value )
				if ( Users::model()->findByPk( $value ) === null )
					$this->addError( $attribute, str_replace( '{attribute}', $attribute_label, $params[ 'message' ] ) );
		}
		else
		{
			if ( Users::model()->findByPk( $this->$attribute ) === null )
				$this->addError( $attribute, str_replace( '{attribute}', $attribute_label, $params[ 'message' ] ) );
		}
	}

	public function checkSameAsSource( $attribute, $params )
	{
		$attribute_label = $this->attributeLabels();
		$attribute_label = $attribute_label[ $attribute ];

		if ( !is_array( $this->$attribute ) )
		{
			if ( $this->$attribute == $this->source_user_id )
				$this->addError( $attribute, str_replace( '{attribute}', $attribute_label, $params[ 'message' ] ) );
		}
	}

	protected function lastTransfersBefore( $limit, $id = null )
	{
		if ( !$limit )
			return array();

		$criteria = new CDbCriteria;

		if ( isset( $id ) )
			$criteria->condition = 'id < '.$id;

		$criteria->alias = 'Transfers';
		$criteria->order = 'id DESC';
		$criteria->limit = $limit;

		return $this->query( $criteria, true );
	}

	protected function findLastByHash( $hash )
	{
		$criteria = new CDbCriteria;

		$criteria->alias = 'Transfers';
		$criteria->compare( 'hash', $hash );
		$criteria->order = 'id DESC';

		return $this->query( $criteria );
	}

	protected function findAllByHash( $hash, $limit = -1, $offset = -1 )
	{
		$criteria = new CDbCriteria;

		$criteria->alias = 'Transfers';
		$criteria->compare( 'hash', $hash );
		$criteria->order = 'id DESC';
		$criteria->limit = $limit;
		$criteria->offset = $offset;

		return $this->query( $criteria, true );
	}

	protected function encryptData()
	{
		$this->setTransferAttributes( $this->getTransferAttributes() );
		
		$this->hash = static::generateHash( $this->_chiperkey );
		$this->data = static::encrypt( serialize( $this->getTransferAttributes() ), $this->_chiperkey );
        $this->data2 = static::encrypt2( json_encode( $this->getTransferAttributes() ), $this->_chiperkey );
	}

	protected function decryptData()
	{
		if ( $this->hash !== static::generateHash( $this->_chiperkey ) )
			return false;

		$this->setTransferAttributes( unserialize( static::decrypt( $this->data, $this->_chiperkey ) ) );
//        $this->setTransferAttributes( CJSON::decode( static::decrypt2( $this->data2, $this->_chiperkey ) ) );

		return $this;
	}

	protected function signTransfer( $signing_records )
	{
		$last_transfers_before = $this->lastTransfersBefore( $signing_records, $this->id );

		$signing_data = '';
		foreach ( $last_transfers_before as $record )
			$signing_data .= $record->data;

		$this->signed_records = 0; //count( $last_transfers_before );
		$this->sign = ''; //static::generateHash( serialize( $this->getTransferAttributes() ).$signing_data );
	}

	protected function checkTransfer()
	{
		return true; // FIXME: CHECK IT!!!

		$sign = $this->sign;
		$signed_records = $this->signed_records;

		$this->sign = null;
		$this->signTransfer( $this->signed_records );

		if ( $sign !== $this->sign || $signed_records !== $this->signed_records )
		{
			$this->sign = $sign;
			$this->signed_records = $signed_records;

			return false;
		}

		return true;
	}

	public function getTotal()
	{
		$last_transfer = $this->findLastByHash( static::generateHash( $this->_chiperkey ) );

		if ( $last_transfer === null )
			return array();

		$last_transfer->_chiperkey = $this->_chiperkey;
		$last_transfer->decryptData();

		if ( !$last_transfer->checkTransfer() )
			throw new CHttpException( 500, 'Несовпадение подписей при проверке переводов. Обратитесь к администратору.' );

		return $last_transfer->total;
	}

	public function getTotalAsObject()
	{
		$total = $this->getTotal();
		$result = array();

		foreach ( $total as $user_id => $value )
		{
			$item = array();

			$item[ 'user' ] = Users::model()->findByPk( $user_id );
			$item[ 'value' ] = $value;

			$result[ ] = (object)$item;
		}

		return $result;
	}

	public function getHistory( $limit = -1, $offset = -1 )
	{
		$this->setChiperKey( $this->_chiperkey );

		$result = $this->findAllByHash( static::generateHash( $this->_chiperkey ), $limit, $offset );

		foreach ( $result as $key => $value )
		{
			$result[ $key ]->_chiperkey = $this->_chiperkey;
			$result[ $key ]->decryptData();
		}

		if ( !empty( $result ) )
			$result[ 0 ]->checkTransfer();

		return $result;
	}

	public function getHistoryCount()
	{
		$this->setChiperKey( $this->_chiperkey );

		return $this->countByAttributes( array(
			'hash' => static::generateHash( $this->_chiperkey )
		) );
	}

	public function getTransfer( $id )
	{
		$this->setChiperKey( $this->_chiperkey );

		$result = $this->findByAttributes( array( 'hash' => static::generateHash( $this->_chiperkey ), 'id' => $id ) );

		if ( $result === null )
			return null;

		$result->_chiperkey = $this->_chiperkey;
		$result->decryptData();
		$result->checkTransfer();

		return $result;
	}

	public function getDebtors()
	{
		$this->setChiperKey( $this->_chiperkey );

		$result = $this->getTotalAsObject();
		array_walk( $result, function( &$item ) { $item->value = round( $item->value, 2 ); } );
		$result = array_filter( $result, function ( $item )
		{
			return ( $item->value > 0.05 ? true : false );
		} );

		return $result;
	}

	public function getCreditors()
	{
		$this->setChiperKey( $this->_chiperkey );

		$result = $this->getTotalAsObject();
		array_walk( $result, function( &$item ) { $item->value = round( $item->value, 2 ); } );
		$result = array_filter( $result, function ( $item )
		{
			return ( $item->value < -0.05 ? true : false );
		} );
		$result = array_map( function ( $item )
		{
			$item->value *= -1;

			return $item;
		}, $result );

		return $result;
	}
	
	public function validate( $attributes = null, $clear_errors = true )
	{
		if ( !parent::validate( $attributes, $clear_errors ) )
			return false;
		
		if ( isset( $data[ 'target_coefs' ] ) && !is_array( $data[ 'target_coefs' ] ) )
		{
			$this->addError( 'target_user_id', 'target_coefs attribute must be an array' );
			return false;
		}
		
		return true;
	}

	public function create( $data )
	{
		$this->attributes = $data;

		if ( !$this->validate() )
			return false;

		if ( is_array( $data[ 'target_user_id' ] ) )
		{
			$coef_summ = 0;
			
			foreach ( $data[ 'target_user_id' ] as $target_user_id )
			{
				if ( !isset( $data[ 'target_coefs' ][ $target_user_id ] ) || !is_numeric( $data[ 'target_coefs' ][ $target_user_id ] ) )
					$data[ 'target_coefs' ][ $target_user_id ] = 1;
				$coef_summ += floatval( $data[ 'target_coefs' ][ $target_user_id ] );
			}

			$model = null;
			foreach ( $data[ 'target_user_id' ] as $target_user_id )
			{
				$single_data = $data;
				$single_data[ 'target_user_id' ] = $target_user_id;
				$single_data[ 'value' ] =
					round( $single_data[ 'value' ] /* / count( $data[ 'target_user_id' ] ) */ *
						floatval( $data[ 'target_coefs' ][ $target_user_id ] ) / $coef_summ, 2 );
				if ( $single_data[ 'source_user_id' ] === $single_data[ 'target_user_id' ] )
					continue;
				$model = new Transfers;
				$result = $model->create( $single_data );
				if ( !$result )
					return false;
			}

			return $model;
		}

		$pair_transfer = new Transfers;
		$pair_transfer->attributes = $data;

		try
		{
			if ( Yii::app()->db->driverName !== 'sqlite' )
				Yii::app()->db->createCommand( 'LOCK TABLES users AS t READ,
			                                           users AS source_user READ,
			                                           users AS target_user READ,
			                                           transfers AS Transfers WRITE;' )->execute();

			// first record

			$pair_transfer->_chiperkey = $pair_transfer->target_user->salt;

			$pair_transfer->timestamp = time();
			$pair_transfer->session_id = Yii::app()->user->session->id;
			$pair_transfer->pair_transfer_id = null;

			$pair_transfer->total = $pair_transfer->getTotal();
			if ( isset( $pair_transfer->total[ $pair_transfer->source_user_id ] ) )
				$pair_transfer->total[ $pair_transfer->source_user_id ] -= $pair_transfer->value;
			else
				$pair_transfer->total[ $pair_transfer->source_user_id ] = -$pair_transfer->value;

			$pair_transfer->signTransfer( static::default_signing_records );

			if ( !$pair_transfer->checkTransfer() )
				throw new CHttpException( 500, 'Несовпадение подписей при создании перевода. Обратитесь к администратору.' );

			$pair_transfer->encryptData();

			if ( !$pair_transfer->save( false ) )
			{
				if ( Yii::app()->db->driverName !== 'sqlite' )
					Yii::app()->db->createCommand( 'UNLOCK TABLES;' )->execute();

				return false;
			}

			// second record

			$this->_chiperkey = $this->source_user->salt;

			$this->timestamp = time();
			$this->session_id = Yii::app()->user->session->id;
			$this->pair_transfer_id = $pair_transfer->id;

			$this->total = $this->getTotal();
			if ( isset( $this->total[ $this->target_user_id ] ) )
				$this->total[ $this->target_user_id ] += $this->value;
			else
				$this->total[ $this->target_user_id ] = $this->value;

			$this->signTransfer( static::default_signing_records );

			if ( !$this->checkTransfer() )
				throw new CHttpException( 500, 'Несовпадение подписей при создании перевода. Обратитесь к администратору.' );

			$this->encryptData();

			if ( !$this->save( false ) )
			{
				$pair_transfer->delete();
				if ( Yii::app()->db->driverName !== 'sqlite' )
					Yii::app()->db->createCommand( 'UNLOCK TABLES;' )->execute();

				return false;
			}

			$pair_transfer->pair_transfer_id = $this->id;

			if ( !$pair_transfer->save( false ) )
			{
				$pair_transfer->delete();
				$this->delete();
				if ( Yii::app()->db->driverName !== 'sqlite' )
					Yii::app()->db->createCommand( 'UNLOCK TABLES;' )->execute();

				return false;
			}

			if ( Yii::app()->db->driverName !== 'sqlite' )
				Yii::app()->db->createCommand( 'UNLOCK TABLES;' )->execute();

			if ( Yii::app()->user->id !== $this->source_user_id && !empty( $this->source_user->email ) )
			{
				$email = Yii::app()->email;
				$email->to = $this->source_user->email;
				$email->view = 'transfer_create_source';
				$email->viewVars = array(
					'transfer' => $this,
				);

				if ( !$email->send() )
                    Yii::log( 'Ошибка при отправке email-сообщения на адрес '.$this->source_user->email, CLogger::LEVEL_ERROR );
			}

			if ( Yii::app()->user->id !== $this->target_user_id && !empty( $this->target_user->email ) )
			{
				$email = Yii::app()->email;
				$email->to = $this->target_user->email;
				$email->view = 'transfer_create_target';
				$email->viewVars = array(
					'transfer' => $this,
				);

				if ( !$email->send() )
                    Yii::log( 'Ошибка при отправке email-сообщения на адрес '.$this->target_user->email, CLogger::LEVEL_ERROR );
			}

			return $this;
		}
		catch ( Exception $exception )
		{
			if ( Yii::app()->db->driverName !== 'sqlite' )
				Yii::app()->db->createCommand( 'UNLOCK TABLES;' )->execute();
			throw $exception;
		}
	}
}

;
