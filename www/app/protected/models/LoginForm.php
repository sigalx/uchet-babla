<?php

class LoginForm extends CFormModel
{
	public $username;
	public $password;
	public $short_session = false;

	private $user_identity;

	public function rules()
	{
		return array(
			array( 'username, password', 'required', 'message' => 'Поле «{attribute}» должно быть заполнено' ),
			array( 'short_session', 'boolean' ),
			array( 'password', 'authenticate' ),
		);
	}

	public function attributeLabels()
	{
		return array(
			'username' => 'Логин',
			'password' => 'Пароль',
			'short_session' => 'Короткая сессия',
		);
	}

	public function authenticate( $attribute, $params )
	{
		if ( !$this->hasErrors() )
		{
			$this->user_identity = new UsernamePasswordIdentity( $this->username, $this->password );
			if ( !$this->user_identity->authenticate() )
				$this->addError( 'password', 'Неверный логин или пароль.' );
		}
	}

	public function login()
	{
		if ( $this->user_identity === null )
		{
			$this->user_identity = new UsernamePasswordIdentity( $this->username, $this->password );
			$this->user_identity->authenticate();
		}

		if ( $this->user_identity->errorCode === BaseIdentity::ERROR_NONE )
			return Yii::app()->user->login( $this->user_identity, $this->short_session );

		return false;
	}
}
