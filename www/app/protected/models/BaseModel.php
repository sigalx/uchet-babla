<?php

class BaseModel extends CActiveRecord
{
	public static function generateHash( $message, $salt = '' )
	{
		if ( !in_array( 'sha512', hash_algos() ) )
			throw new CException( 500, 'Алгоритм SHA-512 не определен на этой платформе.' );

		$message .= $salt.Yii::app()->params[ 'salt' ];

		$result = $message;
		for ( $n = 0; $n < 5000; $n++ )
			$result .= hash( 'sha512', $message, true );

		return hash( 'sha512', $result );
	}

	public static function encrypt( $message, $key )
	{
		$key = hash( 'sha256', $key.Yii::app()->params[ 'salt' ], true );

		$iv_size = mcrypt_get_iv_size( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC );
		$iv = mcrypt_create_iv( $iv_size, MCRYPT_DEV_URANDOM );
		$ciphertext = mcrypt_encrypt( MCRYPT_RIJNDAEL_256, $key, $message, MCRYPT_MODE_CBC, $iv );

		return $iv.$ciphertext;
	}

	public static function decrypt( $ciphertext, $key )
	{
		$key = hash( 'sha256', $key.Yii::app()->params[ 'salt' ], true );

		$iv_size = mcrypt_get_iv_size( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC );
		$iv = substr( $ciphertext, 0, $iv_size );
		$ciphertext = substr( $ciphertext, $iv_size );

		$message = mcrypt_decrypt( MCRYPT_RIJNDAEL_256, $key, $ciphertext, MCRYPT_MODE_CBC, $iv );

		return $message;
	}

    public static function encrypt2( $message, $key )
    {
        $key = hash( 'sha256', $key.Yii::app()->params[ 'salt' ], true );

        $iv_size = mcrypt_get_iv_size( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC );
        $iv = mcrypt_create_iv( $iv_size, MCRYPT_DEV_URANDOM );
        $ciphertext = mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $key, $message, MCRYPT_MODE_CBC, $iv );

        return $iv.$ciphertext;
    }

    public static function decrypt2( $ciphertext, $key )
    {
        $key = hash( 'sha256', $key.Yii::app()->params[ 'salt' ], true );

        $iv_size = mcrypt_get_iv_size( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC );
        $iv = substr( $ciphertext, 0, $iv_size );
        $ciphertext = substr( $ciphertext, $iv_size );

        $message = mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $key, $ciphertext, MCRYPT_MODE_CBC, $iv );

        return $message;
    }
}

;
