<?php

/**
 * Class Users
 * @property integer id
 * @property string login
 * @property string name
 * @property string email
 * @property integer register_time
 */
class Users extends BaseModel
{
	const root_role_id = 1;
	const administrator_role_id = 2;
	const authenticated_role_id = 10;
	const anonymous_role_id = 50;

	public static function model( $className = __CLASS__ )
	{
		return parent::model( $className );
	}

	public function tableName()
	{
		return 'users';
	}

    public function rules()
    {
        return array(
            array('email, name', 'safe'),
        );
    }

	public function relations()
	{
		return array(
			'role' => array( self::BELONGS_TO, 'Roles', 'role_id' ),
			'parties' => array( self::MANY_MANY, 'Parties', 'party_members(user_id, party_id)' ),
			'bank_account' => array( self::HAS_ONE, 'BankAccounts', 'user_id' ),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => 'Login',
			'name' => 'Name',
		);
	}

	public function getName()
	{
		return !empty( $this->name ) ? $this->name : $this->login;
	}

	public function findByLogin( $login )
	{
		return $this->findByAttributes( array( 'login' => $login ) );
	}

	public function findByVkontakteId( $vkontakte_id )
	{
		return $this->findByAttributes( array( 'vkontakte_id' => $vkontakte_id ) );
	}

	public function authenticateByPassword( $login, $password )
	{
		$user = $this->findByLogin( $login );

		if ( $user === null )
			return false;

		if ( $user->hash !== static::generateHash( $login.$password, $user->salt ) )
			return false;

		return $user;
	}

	public static function create( $role_id, $login, $password = null, $cannot_change_password = null, $password_expiration = null )
	{
		$user = new Users;

		$user->role_id = $role_id;
		$user->login = $login;
		$user->register_time = time();
		$user->cannot_change_password = $cannot_change_password;
		$user->password_expiration = $password_expiration;

		$user->salt = static::generateHash( serialize( $user->attributes ).uniqid() );
		$user->hash = $password ? static::generateHash( $login.$password, $user->salt ) : $login;

		if ( !$user->save() )
			return false;

		return $user;
	}
}