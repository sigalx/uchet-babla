<?php

class Parties extends BaseModel
{
	public static function model( $className = __CLASS__ )
	{
		return parent::model( $className );
	}

	public function tableName()
	{
		return 'parties';
	}

	public function rules()
	{
		return array(
			array( 'name', 'required', 'message' => 'Поле «{attribute}» должно быть заполнено' ),

			array( 'email', 'email', 'message' => 'Неверный формат значения' ),

			array( 'name, email', 'safe' ),
		);
	}

	public function relations()
	{
		return array(
			'users' => array( self::MANY_MANY, 'Users', 'party_members(party_id, user_id)' ),
		);
	}

	public function attributeLabels()
	{
		return array(
			'name' => 'Название',
			'uri_name' => 'Ссылка',
			'email' => 'Email партии',
		);
	}

	public function findByUriName( $uri_name )
	{
		return $this->findByAttributes( array( 'uri_name' => $uri_name ) );
	}

	public function create( $data )
	{
		$this->attributes = $data;

		$this->owner_user_id = Yii::app()->user->id;
		$this->register_time = time();
		$this->uri_name = hash( 'crc32b', serialize( $this->attributes ) );

		$this->hash = static::generateHash( serialize( $this->attributes ) );

		if ( !$this->save() )
			return false;

		return $this->joinUser( $this->owner_user_id );
	}

	public function isPartyMember( $id = null )
	{
		if ( empty( $id ) )
			$id = Yii::app()->user->id;

		return Yii::app()->db->createCommand()
			->select( 'id' )
			->from( 'party_members' )
			->where( 'party_id = :party_id AND user_id = :user_id', array( ':party_id' => $this->id, ':user_id' => $id ) )
			->queryRow() !== false;
	}

	public function joinUser( $id = null )
	{
		if ( empty( $id ) )
			$id = Yii::app()->user->id;

		$sql = Yii::app()->db->createCommand();

		return $sql->insert( 'party_members', array( 'party_id' => $this->id, 'user_id' => $id, 'member_since' => time() ) );
	}
}