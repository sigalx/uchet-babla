<?php

class BankAccounts extends BaseModel
{
	public static function model( $className = __CLASS__ )
	{
		return parent::model( $className );
	}

	public function tableName()
	{
		return 'bank_accounts';
	}

	public function rules()
	{
		return array(
			array( 'fullname, account_number, bank_name, bik, correspondent_account, inn, kpp', 'required', 'message' => 'Поле должно быть заполнено' ),

			// length
			array( 'fullname', 'length', 'max' => 100, 'tooLong' => 'Слишком длинное значение (не более 100 символов)' ),
			array( 'account_number', 'length', 'is' => 20, 'message' => 'Неверная длина значения (должно быть 20 символов)' ),
			array( 'bank_name', 'length', 'max' => 100, 'tooLong' => 'Слишком длинное значение (не более 100 символов)' ),
			array( 'bik', 'length', 'is' => 9, 'message' => 'Неверная длина значения (должно быть 9 символов)' ),
			array( 'correspondent_account', 'length', 'is' => 20, 'message' => 'Неверная длина значения (должно быть 20 символов)' ),
			array( 'inn', 'length', 'is' => 10, 'message' => 'Неверная длина значения (должно быть 10 символов)' ),
			array( 'kpp', 'length', 'is' => 9, 'message' => 'Неверная длина значения (должно быть 9 символов)' ),

			// format
			array( 'account_number', 'numerical', 'integerOnly' => true, 'message' => 'Поле должно быть числом' ),
			array( 'bik', 'numerical', 'integerOnly' => true, 'message' => 'Поле должно быть числом' ),
			array( 'correspondent_account', 'numerical', 'integerOnly' => true, 'message' => 'Поле должно быть числом' ),
			array( 'inn', 'numerical', 'integerOnly' => true, 'message' => 'Поле должно быть числом' ),
			array( 'kpp', 'numerical', 'integerOnly' => true, 'message' => 'Поле должно быть числом' ),
		);
	}

	public function relations()
	{
		return array(
			'user' => array( self::BELONGS_TO, 'Users', 'user_id' ),
		);
	}

	public function attributeLabels()
	{
		return array(
			'fullname' => 'ФИО',
			'account_number' => 'Номер счета',
			'bank_name' => 'Банк получателя',
			'bik' => 'БИК:',
			'correspondent_account' => 'Кор. счет',
			'inn' => 'ИНН банка',
			'kpp' => 'КПП банка',
		);
	}
}
