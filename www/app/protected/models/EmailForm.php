<?php

class EmailForm extends CFormModel
{
	public $email;

	public function init()
	{
		parent::init();

		$user = Users::model()->findByPk( Yii::app()->user->id );
		$this->email = $user->email;
	}

	public function rules()
	{
		return array(
			array( 'email', 'required', 'message' => 'Поле «{attribute}» должно быть заполнено' ),
			array( 'email', 'email', 'message' => 'Указан некорректный адрес электронной почты' ),
		);
	}

	public function attributeLabels()
	{
		return array(
			'email' => 'Email',
		);
	}

	public function setEmail()
	{
		$user = Users::model()->findByPk( Yii::app()->user->id );
		$user->email = $this->email;

		return $user->save();
	}
}
