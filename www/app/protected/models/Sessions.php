<?php

class Sessions extends BaseModel
{
	public static function model( $className = __CLASS__ )
	{
		return parent::model( $className );
	}

	public function tableName()
	{
		return 'sessions';
	}

	public function relations()
	{
		return array(
			'user' => array( self::BELONGS_TO, 'Users', 'user_id' ),
		);
	}

	public function findByAuthHash( $auth_hash )
	{
		$session = $this->with( 'user' )->findByAttributes( array(
			'hash' => static::generateHash( $auth_hash, $_SERVER[ 'REMOTE_ADDR' ] )
		) );

		if ( $session === null ||
			$session->close_time && $session->close_time < time()
		)
			return false;

		return $session;
	}

	public static function create( $user_id, $auth_hash, $identity_type, $close_time = null )
	{
		$session = new Sessions;

		$session->user_id = $user_id;
		$session->hash = static::generateHash( $auth_hash, $_SERVER[ 'REMOTE_ADDR' ] );
		$session->identity_type = $identity_type;
		$session->start_time = time();
		$session->close_time = $close_time;
		$session->ipaddr = ipaddrToInt( $_SERVER[ 'REMOTE_ADDR' ] );

		if ( !$session->save() )
			return false;

		return $session;
	}
}