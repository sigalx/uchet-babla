<?php

$email->subject = 'Уведомление о переводе';

?>

Пользователем <?php echo $transfer->session->user->getName(); ?> был создан перевод в пользу <?php echo $transfer->target_user->getName(); ?> от вас в размере <?php echo $transfer->value; ?>.

<?php echo $transfer->comment; ?>
