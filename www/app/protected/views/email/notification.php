<?php

$email->subject = 'Уведомление о задолженностях';

?>

Тов. <?php echo $user->getName(); ?>, в настоящий момент у вас имеются задолженности перед следующими однопартийцами:

<?php foreach ( $creditors as $item ): ?>
<?php echo $item->user->getName() . ' — ' . $item->value . PHP_EOL; ?>
<?php endforeach; ?>

Их нужно когда-нибудь погасить.
