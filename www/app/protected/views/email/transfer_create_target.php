<?php

$email->subject = 'Уведомление о переводе';

?>

Пользователем <?php echo $transfer->session->user->getName(); ?> был создан перевод от <?php echo $transfer->source_user->getName(); ?> в вашу пользу в размере <?php echo $transfer->value; ?>.

<?php echo $transfer->comment; ?>
