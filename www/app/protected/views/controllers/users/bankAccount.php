<?php

$this->pageTitle = 'Указать банковские реквизиты';
$this->breadcrumbs = array(
	$this->pageTitle => array( $this->action->id ),
);

?>

<div><a class="i-am-from-alfa-link">Я из Альфа-Банка!</a></div>

<?php $this->widget( 'XActiveForm', array(
	'id' => 'bank-account-form',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
	),
	'model' => $model,
	'submit' => 'Сохранить',
	'fields' => array
	(
		'fullname' => array( 'type' => 'text' ),
		'account_number' => array( 'type' => 'number' ),
		'bank_name' => array( 'type' => 'text' ),
		'bik' => array( 'type' => 'text' ),
		'correspondent_account' => array( 'type' => 'text' ),
		'inn' => array( 'type' => 'text' ),
		'kpp' => array( 'type' => 'text' ),
	),
) );

?>

<script type="application/javascript">
	function fillAsAlfabankClient()
	{
		$( '#BankAccounts_bank_name' ).val( 'ОАО "Альфа-Банк"' );
		$( '#BankAccounts_bik' ).val( '044525593' );
		$( '#BankAccounts_correspondent_account' ).val( '30101810200000000593' );
		$( '#BankAccounts_inn' ).val( '7728168971' );
		$( '#BankAccounts_kpp' ).val( '775001001' );
	}

	$( function ()
	{
		$( '.i-am-from-alfa-link' ).click( fillAsAlfabankClient );
	} );
</script>
