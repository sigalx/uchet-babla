<?php

$this->pageTitle = 'Указать адрес электронной почты';
$this->breadcrumbs = array(
	$this->pageTitle => array( $this->action->id ),
);

$this->widget( 'XActiveForm', array(
	'id' => 'email-form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
	),
	'model' => $model,
	'submit' => 'Сохранить',
	'fields' => array
	(
		'email' => array( 'type' => 'email' ),
	),
) );

?>
