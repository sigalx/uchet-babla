<?php

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish( 'styles/transfers/index.css' ) );

echo XHtml::link( 'Сводная таблица', array( '/transfers/total' ), array( 'class' => 'big-link float-right' ) );

$first_number_in_page = $pages->itemCount - $pages->limit * $pages->currentPage;
$last_number_in_page = $pages->itemCount - $pages->limit * ( $pages->currentPage + 1 ) + 1;
$last_number_in_page = $last_number_in_page >= 1 ? $last_number_in_page : 1;

?>

<div>

	<div class="mainbar">

		В списке представлены переводы с <?php echo $first_number_in_page; ?>
		по <?php echo $last_number_in_page; ?>:

		<table class="transfer-history">
			<thead>
			<tr>
				<th>Дата</th>
				<th>Источник</th>
				<th>Получатель</th>
				<th>Сумма</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ( $transfers as $transfer ): ?>
				<tr>
					<td><?php echo date( 'd M Y', $transfer->timestamp ); // D, d M Y H:i ?></td>
					<td><?php echo $transfer->source_user->getName(); ?></td>
					<td><?php echo $transfer->target_user->getName(); ?></td>
					<td><?php echo $transfer->value; ?></td>
					<td><?php echo XHtml::link( 'Детали', array( 'view', 'id' => $transfer->id ) ); ?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>

		<?php $this->widget( 'CLinkPager', array(
			'htmlOptions' => array(
				'class' => 'link-pager',
			),
			'header' => 'Страницы: ',
			'firstPageLabel' => 'первая',
			'lastPageLabel' => 'последняя',
			'nextPageLabel' => 'вперед',
			'prevPageLabel' => 'назад',
			'cssFile' => Yii::app()->assetManager->publish( 'styles/pager.css' ),
			'pages' => $pages
		) ); ?>

	</div>

	<div class="sidebar navigation-links">
		<?php $this->renderPartial( '_navigation_links' ); ?>
	</div>

</div>
