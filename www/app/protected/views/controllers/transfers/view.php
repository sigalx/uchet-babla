<?php

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish( 'styles/transfers/view.css' ) );

$this->pageTitle = 'Перевод #'.$model->id;
$this->breadcrumbs = array_merge( $this->breadcrumbs,
	array( $this->pageTitle => array( 'view', 'id' => $model->id ) ) );

?>

<div>

	<table class="transfer-details">
		<tbody>

		<tr>
			<th>ID перевода</th>
			<td><?php echo $model->id; ?></td>
		</tr>

		<tr>
			<th>Кто создал</th>
			<td><?php echo $model->session->user->getName(); ?></td>
		</tr>

		<tr>
			<th>Время и дата</th>
			<td><?php echo date( 'l, d F Y H:i', $model->timestamp ); ?></td>
		</tr>

		<tr>
			<th>Источник перевода</th>
			<td><?php echo $model->source_user->getName(); ?></td>
		</tr>

		<tr>
			<th>Получатель перевода</th>
			<td><?php echo $model->target_user->getName(); ?></td>
		</tr>

		<tr>
			<th>Сумма перевода</th>
			<td><?php echo $model->value; ?></td>
		</tr>

		<tr>
			<th>Комментарий</th>
			<td><?php echo XHtml::encode( $model->comment ); ?></td>
		</tr>

		</tbody>
	</table>


</div>
