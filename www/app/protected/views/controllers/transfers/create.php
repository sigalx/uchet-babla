<?php

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish( 'styles/transfers/create.css' ) );

$this->pageTitle = 'Создать перевод';
$this->breadcrumbs = array_merge( $this->breadcrumbs,
	array( $this->pageTitle => array( 'create' ) ) );

$party_member_items = array( 'empty' => '' );

foreach ( $party_members as $member )
{
	$party_member_items[ $member->id ] = $member->getName();
	if ( $member->id === Yii::app()->user->id )
		$party_member_items[ $member->id ] .= ' (Я)';
}

$this->widget( 'XActiveForm', array(
	'id' => 'transfers-form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
	),
	'model' => $model,
	'fields' => array
	(
		'source_user_id' => array( 'type' => 'dropdown', 'items' => $party_member_items, 'selected' => ( isset( $source_user ) ? $source_user->id : Yii::app()->user->id ) ),
		'target_user_id' => array( 'type' => 'dropdown', 'items' => $party_member_items, 'selected' => ( isset( $target_user ) ? $target_user->id : ( isset( $source_user ) ? Yii::app()->user->id : 'empty' ) ) ),
		'value' => array( 'hint' => 'В качестве дробного разделителя используется точка' ),
		'comment' => array( 'type' => 'textarea' ),
	),
) );

?>

<?php if ( isset( $target_user ) && !empty( $target_user->bank_account ) ): ?>
	<?php
	$labels = BankAccounts::model()->attributeLabels();
	?>
	<p>Реквизиты получателя перевода:</p>
	<table class="bank-account">
		<tbody>
		<?php foreach ( $target_user->bank_account->safeAttributeNames as $attribute ): ?>
			<tr>
				<th><?php echo isset( $labels[ $attribute ] ) ? $labels[ $attribute ] : $attribute; ?></th>
				<td><?php echo $target_user->bank_account->{$attribute}; ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>
