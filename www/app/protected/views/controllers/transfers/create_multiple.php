<?php

$this->pageTitle = 'Создать перевод многим';
$this->breadcrumbs = array_merge( $this->breadcrumbs,
	array( $this->pageTitle => array( 'create' ) ) );

$party_member_items = array();

foreach ( $party_members as $member )
{
	$party_member_items[ $member->id ] = $member->getName();
	if ( $member->id === Yii::app()->user->id )
		$party_member_items[ $member->id ] .= ' (Я)';
}

$this->widget( 'XActiveForm', array(
	'id' => 'transfers-form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
	),
	'model' => $model,
	'fields' => array
	(
		'source_user_id' => array( 'type' => 'dropdown', 'items' => $party_member_items, 'selected' => Yii::app()->user->id ),
		'target_user_id' => array( 'type' => 'checkboxlist', 'items' => $party_member_items ),
		'value' => array( 'hint' => 'Введите ОБЩУЮ сумму перевода' ),
		'comment' => array( 'type' => 'textarea' ),
	),
) );

?>
<style type="text/css">
	.coef-input
	{
		width: 50px;
		height: 20px;
	}
</style>
<script type="application/javascript">
$(function()
{
	$.each( $( '#Transfers_target_user_id > input[type="checkbox"]' ),
	        function( i, val )
	        {
	        	var checkbox = $( val );
	        	var coef_input = $( '<input class="coef-input" type="number" value="1" />' );
	        	coef_input.attr( 'name', 'Transfers[target_coefs][' + checkbox.val() + ']' );
	        	coef_input.insertBefore( checkbox );
	        } );
} );
</script>
