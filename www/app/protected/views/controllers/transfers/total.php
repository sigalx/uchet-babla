<?php

Yii::app()->clientScript->registerCssFile( Yii::app()->assetManager->publish( 'styles/transfers/total.css' ) );

$this->pageTitle = 'Сводная таблица';
$this->breadcrumbs = array_merge( $this->breadcrumbs,
	array( $this->pageTitle => array( 'total' ) ) );

?>

<div>

	<table class="total-lists-united">
		<tbody>
		<tr>
			<td>

				Мне должны:
				<table class="total-list">
					<thead>
					<tr>
						<th>Источник</th>
						<th>Сумма</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ( $debtors as $item )
					{ ?>
						<tr>
							<td><?php echo XHtml::link( $item->user->getName(), array( 'create', 'source_user_id' => $item->user->id ) ); ?></td>
							<td><?php echo $item->value; ?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>

			</td>
			<td>

				Я должен:
				<table class="total-list">
					<thead>
					<tr>
						<th>Получатель</th>
						<th>Сумма</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ( $creditors as $item )
					{ ?>
						<tr>
							<td><?php echo XHtml::link( $item->user->getName(), array( 'create', 'target_user_id' => $item->user->id ) ); ?></td>
							<td><?php echo $item->value; ?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>

			</td>
		</tr>
		</tbody>
	</table>

	<p>Щелкните на имени в списке должников или кредиторов для быстрого открытия формы перевода.</p>

</div>
