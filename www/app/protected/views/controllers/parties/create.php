<?php

$this->pageTitle = 'Основать партию';
$this->breadcrumbs = array_merge( $this->breadcrumbs,
	array( $this->pageTitle => array( 'create' ) ) );

?>

<?php echo $this->renderPartial( '_form', array( 'model' => $model ) ); ?>
