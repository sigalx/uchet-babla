<?php $this->widget( 'XActiveForm', array(
	'id' => 'parties-form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => false,
	),
	'model' => $model,
	'fields' => array
	(
		'name',
		'email' => array( 'type' => 'email' ),
	),
) ); ?>
