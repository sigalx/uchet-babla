<?php

$this->pageTitle = $model->name;
$this->breadcrumbs = array_merge( $this->breadcrumbs,
	array( $this->pageTitle => array( 'view', 'id' => $model->id ) ) );

echo XHtml::link( 'Основать партию', array( 'create' ), array( 'class' => 'big-link float-right' ) );

?>

<div class="mainbar">

	<p>Чтобы пригласить в партию своего камрада, предложите ему пройти по ссылке:<br />

	<h3 class="tab-started"><?php echo XHtml::link( Yii::app()->createAbsoluteUrl( $this->id.'/join', array( 'uid' => $model->uri_name ) ),
			Yii::app()->createAbsoluteUrl( $this->id.'/join', array( 'uid' => $model->uri_name ) ), array( 'class' => 'big-link' ) ) ?></h3></p>

</div>

<div class="sidebar navigation-links">
	<?php $this->renderPartial( '_navigation_links', array( 'model' => $model ) ); ?>
</div>
