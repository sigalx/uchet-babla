<?php

$this->pageTitle = $model->name;
$this->breadcrumbs = array_merge( $this->breadcrumbs,
	array( $this->pageTitle => array( 'view', 'id' => $model->id ) ) );

echo XHtml::link( 'Основать партию', array( 'create' ), array( 'class' => 'big-link float-right' ) );

?>

<div class="mainbar">

	<p>Чтобы вступить в партию «<?php echo $model->name; ?>» нажмите кнопку «Вступить».</p>

	<?php $this->widget( 'XActiveForm', array(
		'fields' => array
		(),
		'buttons' => array
		(
			'submit' => XHtml::submitButton( 'Вступить' ),
		)
	) ); ?>

</div>

<div class="sidebar">
</div>
