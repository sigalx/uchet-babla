<?php

$this->pageTitle = $model->name;
$this->breadcrumbs = array_merge( $this->breadcrumbs,
	array( $this->pageTitle => array( 'view', 'uid' => $model->uri_name ) ) );

echo XHtml::link( 'Пригласить в партию', array( 'invite', 'uid' => $model->uri_name ), array( 'class' => 'big-link float-right' ) );

?>

<div class="mainbar">

	<?php
	if ( empty( $model->users ) )
		echo '<p>В партии никто не состоит :(</p>';
	else
	{
		?>

		<p>Список членов партии:</p>
		<ul class="no-list-style-type">
			<?php
			if ( empty( $model->users ) )
				echo 'В ней никого нет :(';

			foreach ( $model->users as $user )
				echo XHtml::tag( 'li', array(), '<h3>'.$user->getName().'</h3>' );
			?>
		</ul>

	<?php } ?>

</div>

<div class="sidebar navigation-links">
	<?php $this->renderPartial( '_navigation_links', array( 'model' => $model ) ); ?>
</div>
