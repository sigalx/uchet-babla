<?php

echo XHtml::link( 'Основать партию', array( 'create' ), array( 'class' => 'big-link float-right' ) );

?>

<div class="mainbar">

	<?php
	foreach ( $parties as $party )
		echo XHtml::tag( 'h3', array(),
			XHtml::link( $party->name, array( $this->id.'/view/', 'uid' => $party->uri_name ), array( 'class' => 'big-link' ) ).'<br />' );
	?>

</div>

<div class="sidebar">
</div>
