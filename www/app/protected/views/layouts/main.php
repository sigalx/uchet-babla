<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<title><?php echo Yii::app()->name.( $this->pageTitle ? ' — '.XHtml::encode( $this->pageTitle ) : '' ); ?></title>

	<script type="application/javascript">
		var base_url = '<?php echo Yii::app()->baseUrl; ?>';
		var vkauth_url = '<?php echo Yii::app()->createUrl( 'users/vkauth' ); ?>';
	</script>

	<?php
	$cs = Yii::app()->clientScript;

	$cs->registerPackage( 'jquery.ui' );

	$cs->registerScriptFile( Yii::app()->assetManager->publish( 'scripts/common.js' ) );
	$cs->registerScriptFile( Yii::app()->assetManager->publish( 'scripts/vk-auth.js' ) );

	$cs->registerCssFile( Yii::app()->assetManager->publish( 'styles/main.css' ) );
	$cs->registerCssFile( Yii::app()->assetManager->publish( 'styles/links.css' ) );
	$cs->registerCssFile( Yii::app()->assetManager->publish( 'styles/header.css' ) );
	$cs->registerCssFile( Yii::app()->assetManager->publish( 'styles/auth-box.css' ) );
	$cs->registerCssFile( Yii::app()->assetManager->publish( 'styles/breadcrums.css' ) );

	// TODO: remove
	$cs->registerCssFile( Yii::app()->assetManager->publish( 'styles/yii-log.css' ) );

	$main_background = Yii::app()->assetManager->publish( 'images/background/main.jpg' );

	$cs->registerCss( 'background', <<<EOD
		#background-wrapper
		{
			background-image: url('$main_background');
		}
EOD
	);
	?>

</head>
<body>

<div id="vk_api_transport"></div>
<script type="text/javascript">
	window.vkAsyncInit = function ()
	{
		VK.init( { apiId: <?php echo Yii::app()->params['vk_app_id']; ?> } );
	};

	setTimeout( function ()
	{
		var el = document.createElement( 'script' );
		el.type = 'text/javascript';
		el.src = 'http://vkontakte.ru/js/api/openapi.js';
		el.async = true;
		document.getElementById( 'vk_api_transport' ).appendChild( el );
	}, 0 );
</script>

<div id="background-wrapper"></div>

<div id="outer-wrapper">
	<div id="inner-wrapper">

		<div id="header">
			<div id="auth-box" <?php echo Yii::app()->user->isGuest ? 'unauthorized' : 'class="authorized"'; ?>>
				<?php
				if ( !Yii::app()->user->isGuest )
				{
					?>
					<div class="item">Вы зашли как <?php echo Yii::app()->user->name; ?></div>
					<div class="item"><?php echo CHtml::link( 'Выйти', array( '/users/logout' ) ); ?></div>
				<?php
				}
				else
				{
					?>
					<div
						class="item"><?php echo CHtml::link( 'Войти', array( '/users/login' ), array( 'class' => 'big-link' ) ); ?></div>
				<?php
				}
				?>
			</div>

			<div class="menu">
				<?php
				echo XHtml::link( 'Главная', Yii::app()->homeUrl, array( 'class' => 'item' ) );
				echo XHtml::link( 'Мои партии', array( '/parties' ), array( 'class' => 'item' ) );
				echo XHtml::link( 'Мои переводы', array( '/transfers' ), array( 'class' => 'item' ) );
				?>
			</div>

		</div>

		<div id="content">
			<?php
			$system_highlight = Yii::app()->user->getFlash( 'system-highlight' );
			$system_error = Yii::app()->user->getFlash( 'system-error' );

			if ( $system_highlight )
				echo '<div class="flash-highlight ui-state-highlight ui-corner-all">'.$system_highlight.'</div>';
			if ( $system_error )
				echo '<div class="flash-error ui-state-error ui-corner-all">'.$system_error.'</div>';

			if ( !empty( $this->breadcrumbs ) )
				$this->widget( 'zii.widgets.CBreadcrumbs', array(
					'htmlOptions' => array( 'id' => 'breadcrums' ),
					'homeLink' => false,
					'links' => $this->breadcrumbs,
				) );
			else
				echo XHtml::tag( 'h1', array( 'id' => 'page-header' ), $this->pageTitle );

			echo $content;
			?>
		</div>

		<!--div id="copyleft">2013, <a href="http://sigalx.ru">sigalx.ru</a></div-->

	</div>
	<div id="footer-wrapper">

	</div>
</div>

</body>
</html>
